from time import strftime
import requests as rqt
from datetime import datetime, date
import pytz
import json
import os

from requests import api
import apiPI

today = date.today()
hoje = strftime("%m/%d/%Y")

response = rqt.get(f"http://www.yagua.com.br/login/api/apy_ebots.php?user={os.getenv('USER')}&pass={os.getenv('PASS')}&key=OXxBU1NPQ0lBQ0FPIENVTFRVUkFMIERFIFJFTk9WQXw=&arquivo=T&data={hoje}&tp_data=M")
raw_data = response.text

# função para criar dictionary python
def create_dic(vector):
    dic = {
        "id": vector[0],
        "timestamp": vector[1], # para type datetime datetime.strptime(vector[1], '%Y-%m-%d %H:%M:%S.%f')
        "value": float(vector[2])
    }
    return dic

# variáveis
size_raw_data = len(raw_data.split('\n'))
list_of_dic = []

consumo_mensal46 = 0
consumo_mensalC8 = 0
consumo_mensalDE = 0
consumo_mensal87 = 0

for idx in range(0, size_raw_data-1):
    vector = raw_data.split('\n')[idx].split('|')
    list_of_dic.append(create_dic(vector))

# Consumo Mensal
for medidor in list_of_dic:
    if(medidor['id'] == '43F8C8'):
        consumo_mensalC8 += medidor['value']
    elif(medidor['id'] == '43FA46'):
        consumo_mensal46 += medidor['value']
    elif(medidor['id'] == '43F7DE'):
        consumo_mensalDE += medidor['value']
    elif(medidor['id'] == '43F987'):
        consumo_mensal87 += medidor['value']

# Consumo Mensal 43F8C8
print(consumo_mensalC8)        
ptM_C8 = apiPI.getPiPoints('yagua.dia.43F8C8')[0]
print(apiPI.setValue(ptM_C8['WebId'], consumo_mensalC8))

# Consumo Mensal 43FA46
print(consumo_mensal46)        
ptM_46 = apiPI.getPiPoints('yagua.dia.43FA46')[0]
print(apiPI.setValue(ptM_46['WebId'], consumo_mensal46))

# Consumo Mensal 43F7DE
print(consumo_mensalDE)        
ptM_DE = apiPI.getPiPoints('yagua.dia.43F7DE')[0]
print(apiPI.setValue(ptM_DE['WebId'], consumo_mensalDE))

# Consumo Mensal 43F987
print(consumo_mensal87)        
ptM_87 = apiPI.getPiPoints('yagua.dia.43F987')[0]
print(apiPI.setValue(ptM_87['WebId'], consumo_mensal87))

# criação dic to json
# json_object = json.dumps(list_of_dic, indent = 4)
# print(json_object)