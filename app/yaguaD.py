from time import strftime
import requests as rqt
from datetime import datetime, date
import pytz
import json
import os
import apiPI

today = date.today()
hoje = strftime("%m/%d/%Y")

response = rqt.get(f"http://www.yagua.com.br/login/api/apy_ebots.php?user={os.getenv('USER')}&pass={os.getenv('PASS')}&key=OXxBU1NPQ0lBQ0FPIENVTFRVUkFMIERFIFJFTk9WQXw=&arquivo=T&data={hoje}&tp_data=D")
raw_data = response.text

# função para criar dictionary python
def create_dic(vector):
    dic = {
        "id": vector[0],
        "timestamp": vector[1], # para type datetime datetime.strptime(vector[1], '%Y-%m-%d %H:%M:%S.%f')
        "value": float(vector[2])
    }
    return dic

# variáveis
size_raw_data = len(raw_data.split('\n'))
list_of_dic = []

consumo_diario46 = 0
consumo_diarioC8 = 0
consumo_diarioDE = 0
consumo_diario87 = 0

for idx in range(0, size_raw_data-1):
    vector = raw_data.split('\n')[idx].split('|')
    list_of_dic.append(create_dic(vector))


# Consumo Diário
for medidor in list_of_dic:
    if(medidor['id'] == '43F8C8'):
        consumo_diarioC8 += medidor['value']
    elif(medidor['id'] == '43FA46'):
        consumo_diario46 += medidor['value']
    elif(medidor['id'] == '43F7DE'):
        consumo_diarioDE += medidor['value']
    elif(medidor['id'] == '43F987'):
        consumo_diario87 += medidor['value']

# Medidor 43F8C8
print(consumo_diarioC8)
ptCD_C8 = apiPI.getPiPoints('yagua.hh.43F8C8')[0]
print(apiPI.setValue(ptCD_C8['WebId'], consumo_diarioC8))

# Medidor 43FA46
print(consumo_diario46)      
ptCD_46 = apiPI.getPiPoints('yagua.hh.43FA46')[0]
print(apiPI.setValue(ptCD_46['WebId'], consumo_diario46))  

# Medidor 43F7DE
print(consumo_diarioDE)      
ptCD_DE = apiPI.getPiPoints('yagua.hh.43F7DE')[0]
print(apiPI.setValue(ptCD_DE['WebId'], consumo_diarioDE)) 

# Medidor 43F987
print(consumo_diario87)      
ptCD_87 = apiPI.getPiPoints('yagua.hh.43F987')[0]
print(apiPI.setValue(ptCD_87['WebId'], consumo_diarioDE)) 
        
# criação dic to json
# json_object = json.dumps(list_of_dic, indent = 4)
# print(json_object)