import schedule
import time
import os

def job():
    os.system("python yaguaUH.py")
    os.system("python yaguaD.py")
    os.system("python yaguaM.py")

schedule.every(int(os.getenv('FREQUENCIA_EXECUCAO'))).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)