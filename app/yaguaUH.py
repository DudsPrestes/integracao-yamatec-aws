from time import strftime
import requests as rqt
from datetime import datetime, date
import pytz
import json
import os
import apiPI

today = date.today()
hoje = strftime("%m/%d/%Y")

response = rqt.get(f"http://www.yagua.com.br/login/api/apy_ebots.php?user={os.getenv('USER')}&pass={os.getenv('PASS')}&key=OXxBU1NPQ0lBQ0FPIENVTFRVUkFMIERFIFJFTk9WQXw=&arquivo=T&data={hoje}&tp_data=UH")
raw_data = response.text

# Converter data para UTC - America/Sao Paulo
def ConversaoData(dt):
    local = pytz.timezone('America/Sao_Paulo')
    dt = dt.split('.')[0]
    date = datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")
    local_dt = local.localize(date, is_dst=None)
    date = local_dt.astimezone(pytz.utc)
    return date.strftime("%Y-%m-%dT%H:%M:%S")

# função para criar dictionary python
def create_dic(vector):
    dic = {
        vector[0]: {
            "timestamp": ConversaoData(vector[1]), # para type datetime datetime.strptime(vector[1], '%Y-%m-%d %H:%M:%S.%f')
            "value": float(vector[2])
        }
    }
    return dic

# variáveis
size_raw_data = len(raw_data.split('\n'))
list_of_dic = []

for idx in range(0, size_raw_data-1):
    vector = raw_data.split('\n')[idx].split('|')
    list_of_dic.append(create_dic(vector))

print(list_of_dic)

# Medidor 43F7DE
ptMed1 = apiPI.getPiPoints('yagua.ultimahora.43F671')[0]
print(apiPI.setValue(ptMed1['WebId'], list_of_dic[0]['43F671']['value']))
 
# Medidor 43F8C8
ptMed2 = apiPI.getPiPoints('yagua.ultimahora.43F7DE')[0]
print(apiPI.setValue(ptMed2['WebId'], list_of_dic[1]['43F7DE']['value']))

# Medidor 43F987
ptMed3 = apiPI.getPiPoints('yagua.ultimahora.43F8C8')[0]
print(apiPI.setValue(ptMed3['WebId'], list_of_dic[2]['43F8C8']['value']))

# Medidor 43FA46
ptMed4 = apiPI.getPiPoints('yagua.ultimahora.43F987')[0]
print(apiPI.setValue(ptMed4['WebId'], list_of_dic[3]['43F987']['value']))

# Medidor 43F671
ptMed5 = apiPI.getPiPoints('yagua.ultimahora.43FA46')[0]
print(apiPI.setValue(ptMed5['WebId'], list_of_dic[4]['43FA46']['value']))

# criação dic to json
# json_object = json.dumps(list_of_dic, indent = 4)
# print(json_object["43F7DE"]["value"])