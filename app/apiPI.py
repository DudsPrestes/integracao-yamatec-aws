import os
import requests
import json

baseUrl = f"https://{os.getenv('PI_ADDRESS')}/piwebapi"
dataserver = os.getenv('PI_DATASERVER')
scope = os.getenv('PI_SEARCH_SCOPE')
authorization = os.getenv('PI_BASICAUTHORIZATION')

def getPiPoints(name):
    url = baseUrl + f"//search/query"
    params = {"q": f"name:{name}", "scope":scope, "count":1000}
    headers = {  'Authorization': authorization }
    response = requests.request("GET", url, headers=headers, verify=False, params=params)
    response = json.loads(response.text)["Items"]
    return response

def setValue(webid,value):
    url = baseUrl + f"/streams/{webid}/value"
    payload = json.dumps({"Value":value})
    headers = {  'Authorization': authorization, 'Content-Type': 'application/json' }
    return requests.request("POST", url, headers=headers, verify=False, data=payload)
