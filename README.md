![](images/SmartCampus.png) 

<div align = 'justify'>

# Integração Y-Água

## __Descrição Técnica__
Esse projeto consiste na integração de dados sobre o consumo mensal, diário e da última hora que são provenientes dos dispositivos Y-Água.

## __Teoria__ 

### **Python 3 e Suas Bibliotecas**

<img src="images/LOGOPYTHON.png" height="80px">

[Python 3](https://docs.python.org/3/tutorial/index.html) é uma linguagem de programação [orientada a objetos](https://www.alura.com.br/artigos/poo-programacao-orientada-a-objetos), e utiliza identações para diferenciar seu contexto de execução. Neste projeto, ela foi utilizada para a criação de três scripts capazes de fazer requisições do tipo [GET](https://www.w3schools.com/tags/ref_httpmethods.asp) para a [API](https://www.freecodecamp.org/news/what-is-an-api-in-english-please-b880a3214a82/) da Y-Água, obter os dados e então realizar cálculos com os mesmos para gerar um novo valor que possibilita análises estatísticas. Neste projeto foram utilizadas algumas bibliotecas do Python3: [requests](https://docs.python-requests.org/en/master/), [pytz](https://pypi.org/project/pytz/), [time](https://docs.python.org/3/library/time.html), [datetime](https://docs.python.org/3/library/datetime.html).

### __Docker__
 
<img src="images/LOGODOCKER.png" height="100px">

Docker é uma plataforma aberta que permite o desenvolvimento e execução de aplicações dentro de contêineres. Os contêineres possuem a funcionalidade de flexibilizar as aplicações, de forma que estas sejam adaptadas para serem executadas em qualquer dispositivo sem nenhum problema com dependências, bibliotecas ou gerenciadores de pacotes das ferramentas utilizadas.

<img src="images/dockerandvm.jpg">

A imagem acima mostra as principais diferenças entre Máquinas Virtuais e os contêineres Docker. Em uma máquina virtual, existe um sistema operacional núcleo que hospeda os demais sistemas operacionais, e cada um utiliza diferentes ferramentas e dependências para execução de aplicaçõdes. Já em Docker, é necessário apenas um sistema operacional com Docker instalado para que seja possível criar contêineres e construir imagens com todo o conteúdo necessário para executar a aplicação desejada.

### __Rancher__

<img src="images/LOGORANCHER.png" height="90px">

Rancher é uma empresa de software de código aberto, fundada em 2014 na Califórnia por Sheng Liang. O principal objetivo da empresa é possibilitar que organizações possam executar contêineres em produção sem a necessidade de construir uma plataforma de serviços para o contêiner mesclando diversas tecnologias de código aberto. Neste projeto, os serviços do Rancher estão sendo utilizados em um servidor dentro da <a href="https://aws.amazon.com/pt/what-is-aws/">AWS</a>

## __Scripts Envolvidos__

Neste projeto, foram criados alguns scripts para configurar o ambiente Docker:

### [Dockerfile](Dockerfile)
```
FROM python:3 
WORKDIR /app
COPY ./app .
RUN pip install -r requirements.txt
CMD ["python", "./app.py"]
```

- Na **Primeira Linha** (```FROM python:3```), especificamos qual imagem e versão será utilizada, nesse caso foi a imagem do python e a versão foi a 3.  
- Na **segunda linha** (```WORKDIR /app```), especificamos qual o caminho em que a aplicação se encontra dentro do contêiner, nesse caso /app.  
- Na **terceira linha** (```COPY ./app .```), é feita a cópia de tudo o que está na pasta app para a pasta de execução da aplicação.
- Na **Quarta Linha** (```RUN pip install -r requirements.txt```), após especificar o caminho de cópia do conteúdo para o diretório de trabalho, é executado um comando para instalar as dependências necessárias para o funcionamento do projeto, nesse caso as dependências estão contidas em um arquivo chamado **[requirements.txt](app/requirements.txt)**.  
- Na **Quinta Linha** (```CMD ["loop.py"]```), estabelecemos um comando que será executado toda vez que iniciarmos o contêiner, nesse caso será executado o [loop.py](app/loop.py), lembrando que podemos ter apenas um comando "CMD" de execução em um Dockerfile.
- Na **Sexta Linha** (```ENTRYPOINT ["python3"]```), estabelecemos um comando *Entrypoint* que irá trabalhar em conjunto com o CMD quando o contêiner for inicializado (**Obs:** Para utilizar CMD e ENTRYPOINT no Dockerfile, é imprenscindível que ambos estejam em formato JSON).

### [docker-compose](docker-compose.yml)

**IMPORTANTE:** Tenha muito cuidado com a identação ao escrever um arquivo docker-compose, é um fator muito importante para conseguir o funcionamento desejado.

```s
services:
    app:
        build: .
        image: yagua
        volumes:
            - ./app:/app
        environment: 
            - PI_DATASERVER=s0tL5TGyV2r0e9YEgriOLroQUElTRVJWRVI
            - PI_SEARCH_SCOPE=pi:PISERVER
            - PI_BASICAUTHORIZATION=Basic YWRtaW5pc3RyYXRvcjpTbWFydDIwMTch
            - PI_ADDRESS=172.16.230.10
            - USER=facens
            - PASS=facens@21
            - FREQUENCIA_EXECUCAO=5
```

No docker-compose são configurados serviços que criam a estrutura da aplicação, de forma que a mesma possa ser executada em ambientes isolados, tornando-a flexível.
Analisando o Script, temos:
- ```services```: Inicializa a configuração dos serviços;
- ```app```: Nome dado ao serviço;
- ```build```: Procura o Dockerfile no diretório atual para iniciar a construção da imagem;
- ```image```: Nome da imagem que não está configurada no Dockerfile;
- ```volumes```: Específica o caminho de persistência e compartilhamento de dados entre contêineres.
- ```environment```: Configura o ambiente em que o contêiner irá trabalhar.

E o projeto possui também alguns scripts escritos em Python 3 que estão sendo executados dentro de um contêiner:  

Especificação                   | [apiPI.py](app/apiPI.py)
--------------------------------|------------------------------------------
**Função**                      | Estabelecer uma ligação entre o contêiner e a PI Web API para realizar a obtenção e definição de valores.

Especificação                   | [yaguaUH.py](app/yaguaUH.py)
--------------------------------|------------------------------------------
**Função**                      | Obtém os dados referentes ao consumo de água da última hora da API do Y-Água e então os converte para o formato JSON.

Especificação                   | [yaguaD.py](app/yaguaD.py)
--------------------------------|------------------------------------------
**Função**                      | Obtém os dados referentes ao consumo de água diário da API do Y-Água e então os converte para o formato JSON.

Especificação                   | [yaguaM.py](app/yaguaM.py)
--------------------------------|------------------------------------------
**Função**                      | Obtém os dados referentes ao consumo de água mensal da API do Y-Água e então os converte para o formato JSON.

Especificação                   | [app.py](app/app.py)
--------------------------------|------------------------------------------
**Função**                      | Utilizado no ambiente de produção, esse script possui a função de definir o tempo de execução dos scripts de requisição e inserção de dados no PI Server.

Especificação                   | [loop.py](app/loop.py)
--------------------------------|------------------------------------------
**Função**                      | Utilizado somente para testes, esse script possui a função de inicializar o contêiner e mantê-lo em execução para que as demais funcionalidades do projeto sejam testadas.

## __Guia de Implementação__

Primeiramente, clone <a href="https://gitlab.com/smart-campus-facens/integracao-yamatec">este repositório</a> e  certifique-se de ter o Docker (versão >= 20.10.6) instalado na máquina onde o contêiner será executado, e em nosso caso iremos utilizar o editor de código Visual Studio Code para realizar as demonstrações.

O primeiro passo é instalar a extensão do Docker para o Visual Studio Code, de forma que o gerenciamento de imagens e contêineres fique mais interativo. Clique no ícone de extensões do VS Code indicado abaixo:

![](images/vscodext.png)

Em seguida, na barra de pesquisas das extensões, digite "Docker":

![](images/vscodedock.png)

Clique na extensão indicada acima e a seguinte tela deve aparecer:

![](images/installdock.png)

Clique no pequeno botão verde com o texto "install" e pronto, a extensão Docker será instalada.

Agora veja que é possível acessar uma nova aba no menu através do ícone do Docker:

![](images/dockericon.png)

Após identificar o ícone e verificar se está tudo correto, utilizando o atalho Ctrl + ', abra o terminal integrado do VS code e digite o seguinte comando para executar o arquivo docker-compose.yml e subir o contêiner:

![](images/vsterminal.PNG)

Quando o comando for executado com sucesso, clique no ícone do Docker mostrado acima e a seguinte aba deverá aparecer:

![](images/vscontainer.png)

Nela é possível visualizar as imagens e contêineres que foram criados.  

Para acessar o contêiner do nosso projeto, Estação Meteorológica, clique sobre o nome do projeto com o botão direito do mouse, e em seguida "Attach Shell":

![](images/vsshell.png)

Isto fará com que o terminal do contêiner apareça e possibilitará a execução dos arquivos dentro dele:

![](images/containerterminal.PNG)

Para mais informações sobre comandos e dicas do Docker, utilize a <a href="https://gist.github.com/bradtraversy/89fad226dc058a41b596d586022a9bd3">Gist do Github</a> deixada na seção de referências deste documento.

# __PI System__

<img src="images/LOGOOSI.png" height="90px"> 

O PI System™ é uma infraestrutura empresarial aberta que conecta dados baseados em sensores, sistemas e pessoas. O resultado é: informações acionáveis e em tempo real que capacitam as empresas a otimizar e transformar seus negócios.

### __PI Web API__
Neste projeto, a PI Web API é utilizada para obter os PI Points e inserir dados nos mesmos.

Link da PI Web API: https://172.16.230.10/piwebapi

Login: ```Administrator```  
Senha: ```Smart2017!```

### __PI Data Archive__

No PI Data Archive, foram criados 15 Pi Points para o projeto Integração Y-Água:

![](images/yda.PNG)

### __PI Asset Framework__

No PI Asset Framework, foram criadas as seguintes árvores de tags:

![](images/yaf.PNG)

### __PI Vision__

Visualização criada para o projeto:

![](images/yvision.PNG)

## __Resultados Esperados__
Os dados fornecidos pela API criada pelo Y-Água devem ser persistidos e apresentados no dashboard do Smart Campus.

## __Ponto de Atenção__
Para utilizar as mesmas versões das ferramentas e suas dependências utilizadas neste projeto, é recomendado utilizar a mesma imagem Docker para subir um contêiner. A imagem pode ser encontrada [neste repositório](https://gitlab.com/smart-campus-facens/integracao-yamatec/container_registry), na aba "Packages and Registries" -> "Container Registry".   
Local de instalação dos medidores:
- Medidor 43F987: Medidor SAAE
- Medidor 43FA46: Lago
- Medidor 43F8C8: Poço
- Medidor 43F7DE: Poço Caipira
- Medidor 43F671: Não Instalado Ainda

## __Referências__
[OSI soft](https://www.osisoft.pt/about-osisoft/)  
[Documentação Python3](https://docs.python.org/3/tutorial/index.html)  
[Login Yagua](https://yagua.com.br/login/)  
[Acessos](https://facens-company.monday.com/boards/956245877/pulses/546540203)
[Documentação do Docker](https://docs.docker.com/)  
[Docker: Comandos, Ajuda e Dicas](https://gist.github.com/bradtraversy/89fad226dc058a41b596d586022a9bd3)     
[Referência para utilização da PI Web API](https://techsupport.osisoft.com/Documentation/PI-Web-API/help/getting-started.html)  
[Página de aprendizado da OSIsoft](https://learning.osisoft.com/page/pt)  
[Definindo o PI System](https://www.osisoft.pt/#:~:text=O%20PI%20System%20%C3%A9%20a,de%20mercado%20para%20opera%C3%A7%C3%B5es%20industriais.&text=Desde%20a%20integra%C3%A7%C3%A3o%20e%20treinamento,lo%20a%20atingir%20suas%20metas.)  
[Guia de utilização do Rancher](https://rancher.com/docs/rancher/v2.x/en/quick-start-guide/)  
[Como funciona uma API](https://technologyadvice.com/blog/information-technology/how-to-use-an-api/)  
[Como funciona a AWS](https://aws.amazon.com/pt/what-is-aws/)  
[O que é uma Estação Meteorológica](https://agrosmart.com.br/blog/estacao-meteorologica-funciona-importancia-agricultura/)  

## __Autor__  
Centro Universitário Facens 

Equipe Técnica *Smart Campus* 
Facens:  
Matheus Avilla Vial  
</div>